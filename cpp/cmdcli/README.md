# cmdcli
A CMake compiled launcher for espeak and cowsay with more features.

## Features
There is currently one feature: to type "exit" to exit programs.

## Build Directions
To build on Linux, install cmake, make, and g++

### Then Complie

```mkdir build```

```cd build```

```cmake ..```

```make```

### Then Enjoy :smiley: